

/**
 * 
 *
 *      +-----------------------------------------------------------------------------------------------------------------+ 
 *      |                                             Copyright - NWMSU                                                   |
 *      |                                           NWMSU INTERNAL USE ONLY                                               |
 *      |-----------------------------------------------------------------------------------------------------------------| 
 *      |Team 05                                                                                                          |
 *      |Unit 05 : Purna Medarametla, Lakshman Pavan Kumar Vudutha                                                                                  |
 *      |Unit 08 : Chinta Raja Srikar Karthik, Chandar Mouli Kantipudi                                                    |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program uses mocha and node-mocks to test "estimate" controller.                               |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                        |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Chinta Raja Srikar Karthik       0.0.1(Initial)       Initial code added  to test estimate controllers.          |
 *      |                                                                                                                 |             
 *      |                                                                                                                 |
 *      +-----------------------------------------------------------------------------------------------------------------+
 */
process.env.NODE_ENV = 'test'
var controller = require('../../controllers/estimate'),
  mocks = require('node-mocks-http'),
  should = require('should'),
  nock = require('nock')

const LOG = require('../../utils/logger.js')

var request = require('request');
const expect = require('chai').expect
// var request = require('supertest')
LOG.debug('Starting test/controllers/estimateTest.js.')

function buildResponse() {
  return mocks.createResponse({

    eventEmitter: require('events').EventEmitter
  })
}

describe('Services Controller Tests', function () {
  beforeEach(function (done) {
    this.response = mocks.createResponse({
      eventEmitter: require('events').EventEmitter
    })
    done()
  })

  /**
   * Team 05 
   * Testing estimate/ controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/', function (done) {
    request('http://localhost:8089/estimate/', function (error, response, body) {
      console.log('Testing default estimate controller:')
      console.log('Expected Result: 200')      
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()

    });
  })
  /**
   * Team 05 
   * Testing estimate/edit/:id controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/edit/:id', function (done) {
    request('http://localhost:8089/estimate/edit/1', function (error, response, body) {
      console.log('Testing estimate/:id controller:')
      console.log('Expected Result: 200')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()
    });
  })
  /**
   * Team 05 
   * Testing estimate/save controller using GET Method
   * Expected Result: response status code == 404
   */
  it('GET: estimate/save', function (done) {
    request('http://localhost:8089/estimate/save', function (error, response, body) {
      console.log('Testing estimate/save controller:')
      console.log('Expected Result: 404')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(404)
      done()
    });
  })
    /**
   * Team 05 
   * Testing estimate/delete controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/delete:id', function (done) {
    request('http://localhost:8089/estimate/delete/1', function (error, response, body) {
      console.log('Testing estimate/save controller:')
      console.log('Expected Result: 200')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()
    });
  })
    /**
   * Team 05 
   * Testing estimate/details controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/details:id', function (done) {
    request('http://localhost:8089/estimate/details/1', function (error, response, body) {
      console.log('Testing estimate/save controller:')
      console.log('Expected Result: 200')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()
    });
  })
    /**
   * Team 05 
   * Testing estimate/findall controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/findall', function (done) {
    request('http://localhost:8089/estimate/findall', function (error, response, body) {
      console.log('Testing estimate/save controller:')
      console.log('Expected Result: 200')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()
    });
  })
    /**
   * Team 05 
   * Testing estimate/findone controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/findone:id', function (done) {
    request('http://localhost:8089/estimate/findone/1', function (error, response, body) {
      console.log('Testing estimate/save controller:')
      console.log('Expected Result: 200')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()
    });
  })
    /**
   * Team 05 
   * Testing estimate/create controller using GET Method
   * Expected Result: response status code == 200
   */
  it('GET: estimate/create', function (done) {
    request('http://localhost:8089/estimate/create', function (error, response, body) {
      console.log('Testing estimate/save controller:')
      console.log('Expected Result: 200')
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('Results:')
      if(error) return done(error)
      expect('Content-type', 'text/html; charset=utf-8')
      expect('X-Powered-By', 'Express')
      expect(response.body.error).equal(undefined)
      expect(response.statusCode).to.be.equal(200)
      done()
    });
  })
})