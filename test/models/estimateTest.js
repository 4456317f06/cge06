/**
 *
 *      +-----------------------------------------------------------------------------------------------------------------+ 
 *      |                                             Copyright - NWMSU                                                   |
 *      |                                           NWMSU INTERNAL USE ONLY                                               |
 *      |-----------------------------------------------------------------------------------------------------------------| 
 *      |Team 04                                                                                                          |
 *      |Unit 04 : Prem Kiran Osipalli  , ujjawal Kumar                                                                   |
 *      |Unit 09 : Ashok Atkuri, Venkata Siva Sai Nadipeneni, Sai Kumar Uppala                                            |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program uses mocha and node-mocks to test "estimate" model.                                    |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                        |
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Ashok Atkuri       0.0.1(Initial)       Initial code added to test Estimate models.                              |
 *      |                                                                                                                 |             
 *      |                                                                                                                 |
 *      +-----------------------------------------------------------------------------------------------------------------+
 */

var expect = require('chai').expect;

var Estimate = require('../../models/estimate');

describe('Estimate', function() {
    it('should be invalid if name is empty', function(done) {
        var m = new Estimate();
        m.validate(function(err) {
            expect(m.name).to.be.a('String');
            done();
        });
    });
});

describe('estimate', function() {
	it('should be invalid if id is empty', function(done) {
		var e = new Estimate();

		e.validate(function(err) {
			expect(err.errors._id).to.exist;
			done();
		});
	});
});



describe('estimate', function() {
    it('should be invalid if squareFeet is String', function(done) {
        var m = new Estimate();
 
        m.validate(function(err) {
            expect(m.squareFeet).to.be.a('Number');
            done();
        });
    });
});

describe('estimate', function() {
    it('should be invalid if multiplier is String', function(done) {
        var m = new Estimate();
 
        m.validate(function(err) {
            expect(m.multiplier).to.be.a('Number');
            done();
        });
    });
});