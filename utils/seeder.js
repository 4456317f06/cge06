//Well You have gotta contact Team 6 - Unit 7(Vineeth & Sangeetha) for this//
// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')
const estimates = require('../data/estimates.json')
const users = require('../data/users.json')
const UserModel = require('../models/user.js')

module.exports = (app) => {
 LOG.info('START seeder.')
 const db = new Datastore()
 db.loadDatabase()

 // insert the sample data into our data store
 db.insert(estimates)

 const newUser = function (newUser) {
    LOG.debug('Registering ' + newUser.email)
    const user = new UserModel(newUser)
  
    UserModel.findOne({ email: newUser.email }, (err, existingUser) => {
      if (err) {
        LOG.error(err.message)
        return
      }
      if (existingUser) {
        LOG.debug('User already exists: ', user.email)
        return
      }
      user.save()
      LOG.info(`User with email: ${newUser.email} successfully registered.`)
    })
  }
 users.data.forEach((user) => {
    newUser(user)
  })

 // initialize app.locals (these objects will be available to our controllers)
 app.locals.estimates = db.find(estimates)
 app.locals.users = db.find(users)

 LOG.debug(`${estimates.length} estimates`)
 LOG.info('END Seeder. Sample data read and verified.')
}
