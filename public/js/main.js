/*This is main.js file*/
// function toCurrency(amount) 

// https://gist.github.com/kylefox/780065
function toCurrency(amount) {
    return "$" + amount.toFixed(2);
};

// function toCurrencyInt(amount) 
function toCurrencyInt(amount) {
    return "$" + amount.toFixed(0);
};

// Add  .toCurrency() method to all Numbers
Number.prototype.toCurrency = function () {
    return toCurrency(this);
};

// Add  .toCurrencyInt() method to all Numbers
Number.prototype.toCurrencyInt = function () {
    return toCurrencyInt(this);
};

function onMaterialChange() {
    var count = 1; var sum = 0;
    while (count > 0) {
        if ($('#materialName' + count).length > 0) {
            var materialVal; var materialunits;
            if ($('#Sqftperunit' + count).val() == 0) { materialVal = 0; materialunits = 0; }
            else {
                materialVal = parseInt($("#sqft").val() * $("#materialUnitCost" + count).val() / $('#Sqftperunit' + count).val());
                materialunits = parseInt($("#sqft").val() / $('#Sqftperunit' + count).val());
            }
            $('#individualMaterialCost' + count).html(materialunits + " units = $ " + materialVal);
            count++;
            sum += materialVal;
        }
        else {
            count = 0;
        }
        $("#materialsum").html(sum);
        TotalCost();
    }
}

function onMiscChange() {
    var sum = 0; count = 1;
    while (count > 0) {
        if ($("#miscellaneousName" + count).length > 0) {
            var x=parseInt($("#miscellaneousCost" + count).val());
            if(x>=0)
            sum += x;
            count++;
        }
        else { count = 0; }
    }
    $("#miscellaneoussum").html(sum);
    TotalCost();
}


function onLaborChange() {
    var newlaborcost = $('#numPeople').val() * $('#numDays').val() *
        $('#hoursWorked').val() * $('#laborHourlyCharge').val();
    $("#laborcost").html(newlaborcost);
    onFoodChange(); TotalCost();
}

function onHotleChange() {
    var newhotelcost = $('#numrooms').val() * $('#numnights').val() *
        $('#roomcost').val();
    $("#hotelsum").html(newhotelcost); TotalCost();
}
function onFoodChange() {
    var people = $('#numPeople').val();
    var days = $('#numDays').val();
    $("#numPeopleFood").val(people);
    $("#numDaysFood").val(days);
    var newfoodcost = people * days * $('#foodcostperday').val();
    $("#foodsum").html(newfoodcost); TotalCost();
}
function onVehicleChange() {
    var newvehiclecost = $('#numberOfVehicles').val() * $('#milesPerVehicle').val() *
        $('#dollarsPerMile').val();
    $("#vehiclecost").html(newvehiclecost);
    TotalCost();
}


function TotalCost() {
    var Totalcost = parseInt($('#materialsum').html()) + parseInt($('#laborcost').html()) +
        parseInt($('#foodsum').html()) + parseInt($('#vehiclecost').html()) +
        parseInt($('#miscellaneoussum').html());
    $("#totalcost").html(Totalcost);
    $("#totalcostpersqft").html(Math.round(Totalcost / $('#sqft').val()));
    onMultiplierChange();
}

function onMultiplierChange()
{
var totalbid= parseInt($("#totalcost").html())+parseInt($("#totalcost").html())*$("#multiplier").val();
var totalbidpersqft=Math.round((totalbid/$("#sqft").val()*100)/100);
$("#bidprice").html(totalbid);
$("#bidpricepersqft").html(totalbidpersqft);
}


function onsqftChange() {
    onMaterialChange();
}

// window.onload = a function that will call all update functions to display calculated values
window.onload = function () {
    onsqftChange();
}

// prevent submission when hitting enter key - optional, but user-friendly 
// document.forms[0].onkeypress = function (e) {
//     var key = e.charCode || e.keyCode || 0;
//     if (key == 13) {
//         e.preventDefault();
//     }
// }
function addInput(divName) {
    var count = 1; var num = 0; var mateormisc = "materialName";
    if (divName == "dynamicMiscellaneousInput") {
        mateormisc = "miscellaneousName";
    }
    while (count > 0) {
        if ($("#" + mateormisc + count).length > 0) {
            count++;
            num = count;
        }
        else { count = 0; }
    }
    var newdiv = document.createElement('div');
    if (divName == "dynamicMiscellaneousInput") {
        newdiv.innerHTML = "<div class=row><div class='col col-xs-6'><div class='form-group'><input type='text' class='form-control' id='miscellaneousName" + num + "' name='miscName' value='Miscellaneous entry " + num + "'></div></div><div class='col col-xs-6'><div class='form-group'><input type='number' onkeyup='onMiscChange()' class='form-control' id='miscellaneousCost" + num + "' name='miscCost' value='0'></div></div></div>";
        $("#miscellaneousCount").html(parseInt($("#miscellaneousCount").html()) + 1);
    } else if (divName == 'dynamicMaterialInput') {
        newdiv.innerHTML = "<div class=row><div class='col col-xs-4'><div class='form-group'><input type='text' class='form-control' id='materialName" + num + "' name='materialName' value='New Material " + num + "'></div></div><div class='col col-xs-4'><div class='form-group'><input onkeyup='onMaterialChange()' type='number' class='form-control' id='materialUnitCost" + num + "' name='materialUnitCost' value=0></div></div><div class='col col-xs-4'><div class='form-group'> <input onkeyup='onMaterialChange()' type='number' class='form-control' id='Sqftperunit" + num + "' name='Sqftperunit' value=0></div></div></div>" +
            "<div> <h4> <span id='individualMaterialCost" + num + "'> 0 units = $ 0</span></h4></div>";
        $("#materialCount").html(parseInt($("#materialCount").html()) + 1);
    }
    document.getElementById(divName).appendChild(newdiv);
}
  // END CLIENT SIDE CODE  (close the script tag)