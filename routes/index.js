/*
*TEAM -5 
*UNIT-5
*Person 1 - Purna Medarametla
*Person 2 - Lakshman Pavan Kumar Vudutha
*/
/**
* @index.js - manages all routing
*
* router.get when assigning to a single request
* router.use when deferring to a controller
*
* @requires express
*/

const express = require('express')
const LOG = require('../utils/logger.js')  // comment out until exists
LOG.debug('START routing')   // comment out until exists
const router = express.Router()
var api_key = 'key-58373f910b52cb87142f833f3b646658';
var domain = 'sandbox2f6c24e87b0f4ad6aa021d50908cd8cb.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

// Manage top-level request first
router.get('/', (req, res, next) => {
 LOG.debug('Request to /')  // comment out until exists
 res.render('index.ejs', { title: 'CGE06' }) //  use your sec num
})

// Defer path requests to a particular controller
router.use('/about', require('../controllers/about.js'))
router.use('/estimate', require('../controllers/estimate.js'))


//Unit 12 addition for contact page
router.get('/contact', (req, res, next) => {
    LOG.debug('Request to /contact')  // comment out until exists
    res.render('contact.ejs', { title: 'Contact Us'  , message : ""  }) 
})


router.post('/contact',function(req,res){
    var data = {
      from: 'C&G Coatings <curt.carpenter0@gmail.com>',
      to: 's528749@nwmissouri.edu',
      subject: 'Contact us query',
      text: 'Hi Admin,\n' +
            "Customer : "+ req.body.name +
            "\nCustomer Email: "+ req.body.email +
            "\n\nMessage : "+ req.body.message
    };
     
    mailgun.messages().send(data, function (error, body) {
      console.log(body);
      return res.render('contact.ejs', { title: 'Register', layout: 'layout.ejs', message : "Thanks for contacting us" });
      //res.send({status:400 , message:"Thanks for contacting us"});
    });
    
  })

//Unit 08 Chinta Raja Srikar Karthik for Statistics page
router.use('/statistics', require('../controllers/statistics.js'))


//Unit 12 addition for user pages
router.use('/user', require('../controllers/user.js')) 


LOG.debug('END routing')
module.exports = router
