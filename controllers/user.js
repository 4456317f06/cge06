const express = require('express')
const api = express.Router()
const app = express()
const UserModel = require('../models/user.js')
const passport = require('passport')
const passportConfig = require('../config/passportConfig.js')
const LOG = require('../utils/logger.js')

// GET Register page
api.get('/signup', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('account/signup.ejs', { title: 'Register', layout: 'layout.ejs' , message : ""})
})

api.post('/signup', (req, res) => {
    console.log(`Handling GET ${req}`)
    const data = req.app.locals.users.query.data;
    const item = new UserModel()
    item.email = req.body.email;
    item.password = req.body.password;
    console.log(item)
    data.push(item)
    console.log( JSON.stringify(data))
    item.save(function (err) {
      if (err)  {
        return res.render('account/signup.ejs', { title: 'Register', layout: 'layout.ejs', message : "Invalid usename/password." });
      }else{
        return res.render('account/signup.ejs', { title: 'Register', layout: 'layout.ejs', message : "User registered succesfully." });
      }
      // saved!
    })
    
  })

  //GET Login page
  // GET Register page
api.get('/login', (req, res) => {
  if (req.user) {
    return res.redirect('/')
  }
  res.render('account/login', {
    title: 'Login'
  })
})

api.post('/login', (req, res, next) => {
  req.assert('email', 'Email is not valid').isEmail()
  req.assert('password', 'Password cannot be blank').notEmpty()
  req.sanitize('email').normalizeEmail({ remove_dots: false })
  const errors = req.validationErrors()

  if (errors) {
    req.flash('errors', errors)
    return res.redirect('/user/login')
  }

  passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err) }
    if (!user) {
      LOG.info(info)
      req.flash('errors', info)
      return res.redirect('/user/login')
    }
    req.logIn(user, (err) => {
      if (err) { return next(err) }
      req.flash('success', 'Success! You are logged in.')
      LOG.error(err)
      res.redirect(req.session.returnTo || '/')
    })
  })(req, res, next)
})

api.get('/logout', (req, res) => {
  req.logout()
  req.flash('success', 'Successfully logged out.')
  res.redirect('/user/login')
})


  //Team Unit 08 
  api.get('/forgot', (req, res) => {
    console.log(`Handling GET ${req}`)
    res.render('account/forgot.ejs', { title: 'Forgot', layout: 'layout.ejs' })
  })
  api.post('/forgot', (req, res) => {
    console.log(`Handling GET ${req}`)
    res.render('forgot.ejs', { title: 'Forgot', layout: 'layout.ejs' })
  })
  //GET Progile page
api.get('/profile',passportConfig.isAuthenticated , (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('account/profile.ejs', { title: 'Profile', layout: 'layout.ejs' })
  }) 

/**
 * POST /user/profile
 * Update profile information.
 */
api.post('/profile', passportConfig.isAuthenticated, (req, res, next) => {
  req.assert('email', 'Please enter a valid email address.').isEmail()
  req.sanitize('email').normalizeEmail({ remove_dots: false })

  const errors = req.validationErrors()

  if (errors) {
    req.flash('errors', errors)
    return res.redirect('/user/profile')
  }

  UserModel.findById(req.user.id, (err, user) => {
    if (err) { return next(err) }
    user.email = req.body.email || ''
    user.profile.name = req.body.name || ''
    user.save((err) => {
      if (err) {
        if (err.code === 11000) {
          req.flash('error', 'The email address you have entered is already associated with an account.')
          return res.redirect('/user/profile')
        }
        return next(err)
      }
      req.flash('success', 'Profile information has been updated.')
      res.redirect('/user/profile')
    })
  })
})

/**
 * POST /user/password
 * Update current password.
 */
api.post('/password', passportConfig.isAuthenticated, (req, res, next) => {
  req.assert('password', 'Password must be at least 4 characters long').len(4)
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password)

  const errors = req.validationErrors()

  if (errors) {
    req.flash('errors', errors)
    return res.redirect('/user/profile')
  }

  UserModel.findById(req.user.id, (err, user) => {
    if (err) { return next(err) }
    user.password = req.body.password
    user.save((err) => {
      if (err) { return next(err) }
      req.flash('success', 'Password has been changed.')
      res.redirect('/user/profile')
    })
  })
})


/**
 * POST /user/delete
 * Delete user account.
 */
api.post('/delete', passportConfig.isAuthenticated, (req, res, next) => {
  UserModel.remove({ _id: req.user.id }, (err) => {
    if (err) { return next(err) }
    req.logout()
    req.flash('success', 'Your account has been deleted.')
    res.redirect('/user/login')
  })
})

api.get('/reset', (req,  res)  =>  {
  console.log(`Handling GET ${req}`)
  res.render('account/reset.ejs', { title: 'reset', layout:  'layout.ejs'  })
}) 
api.post('/reset', (req, res) => {
  console.log(`Handling GET ${req}`)
  res.render('reset.ejs', { title:  'reset', layout: 'layout.ejs' })
}) 

module.exports = api