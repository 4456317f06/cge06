/*10 standard CRUD request handlers per controller 

controllers/estimate.js  
 // team 3

2 Respond with JSON:

http://127.0.0.1:8089/estimate/findall       [404]
http://127.0.0.1:8089/estimate/findone/1  [404]

5 Respond with CRUD Views:

http://127.0.0.1:8089/estimate		      [404]
http://127.0.0.1:8089/estimate/create            [404]
http://127.0.0.1:8089/estimate/delete/1         [404]
http://127.0.0.1:8089/estimate/details/1	      [404]
http://127.0.0.1:8089/estimate/edit/1	      [404]

3 Respond by executing CRUD actions:

http://127.0.0.1:8089/estimate/save	[404]
http://127.0.0.1:8089/estimate/save/1 	[404]
http://127.0.0.1:8089/estimate/delete/1 	[404]

*/

const express = require('express')
const api = express.Router()
const Model = require('../models/estimate.js')
const LOG = require('../utils/logger.js') // comment out until exists
const find = require('lodash.find')
const remove = require('lodash.remove')
const passport = require('../config/passportConfig.js')
const notfoundstring = 'estimates'




//findall   -- Unit 2 -- team 2
api.get('/findall', passport.isAuthenticated, function (req, res) {
  res.setHeader('Content-Type', 'application/json')
  console.log(req.app.locals.estimates)
  const data = req.app.locals.estimates.query
  res.send(JSON.stringify(data))
})

//findone   -- Unit 3 -- team 3
api.get('/findone/:id', passport.isAuthenticated, (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  console.log("===============================================")
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

// Index  -- Unit 1 -- team 1
api.get('/', passport.isAuthenticated, function (req, res) {
  res.render('estimate/index.ejs')
})


//Create   -- Unit 4 -- team 4
api.get('/create', passport.isAuthenticated, function (req, res) {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('estimate/create.ejs',
    {
      title: 'Create estimate',
      layout: 'layout.ejs',
      estimate: item
    })
})



//Delete    -- Unit 7 -- team 6 
api.get('/delete/:id', passport.isAuthenticated, function (req, res) {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('estimate/delete.ejs',
    {
      title: 'Delete estimate',
      layout: 'layout.ejs',
      estimates: item
    })
})


//Details   -- Unit 1 -- team 1
api.get('/details/:id', passport.isAuthenticated, function (req, res) {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  var  errStr  =  ''
  if  (!item) {
    errStr  =  'Sorry, requested estimate with id:' + id + ' is not present.'
    //return res.end(notfoundstring) 
  }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return  res.render('estimate/details.ejs',
    {
      title:  'Estimate Details',
      layout:  'layout.ejs',
      estimate:  item,
      errString: errStr
    })

})


//Edit    -- Unit 8 -- team 5
api.get('/edit/:id', passport.isAuthenticated, function (req, res) {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('estimate/edit.ejs',
    {
      title: 'estimates',
      layout: 'layout.ejs',
      estimate: item
    })

})


//insert new -- Unit 12 -- team 1
api.post('/save', passport.isAuthenticated, function (req, res) {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.estimates.query;
  const item = new Model()
  LOG.info(`NEW ID ${req.body._id}`)
  item._id = parseInt(req.body._id, 10) // base 10
  item.name = req.body.name
  item.location = req.body.location
  item.squareFeet = parseInt(req.body.sqft, 10)
  item.materials = []
  item.materials.length = 0
  if (req.body && req.body.materialName && req.body.materialName.length > 0) {
    for (let count = 0; count < req.body.materialName.length; count++) {
      item.materials.push(
        {
          product: req.body.materialName[count],
          unitcost: parseInt(req.body.materialUnitCost[count], 10),
          coverageSquareFeetPerUnit: parseInt(req.body.materialSqft[count], 10)
        }
      )
    }
  }
  item.numberOfPeople = parseInt(req.body.numberOfPeople, 10);
  item.numberOfDays = parseInt(req.body.numberOfDays, 10);
  item.hoursWorkedPerDay = parseInt(req.body.hoursWorkedPerDay, 10);
  item.laborDollarsPerHour = parseInt(req.body.laborDollarsPerHour, 10);
  item.numberHotelNights = parseInt(req.body.numberHotelNights, 10);
  item.hotelDollarsPerNight = parseInt(req.body.hotelDollarsPerNight, 10);
  item.numberHotelRooms = parseInt(req.body.numberHotelRooms, 10);
  item.foodDollarsPerDay = parseInt(req.body.foodDollarsPerDay, 10);
  item.numberOfVehicles = parseInt(req.body.numberOfVehicles, 10);
  item.milesPerVehicle = parseInt(req.body.milesPerVehicle, 10);
  item.dollarsPerMile = parseInt(req.body.dollarsPerMile, 10);
  item.miscellaneous = []
  item.miscellaneous.length = 0
  if (req.body && req.body.misc && req.body.misc.length > 0) {
    for (let count = 0; count < req.body.misc.length; count++) {
      item.miscellaneous.push(
        {
          misc: req.body.misc[count],
          cost: parseInt(req.body.miscCost[count], 10)

        }
      )
    }
  }
  item.multiplier = parseInt(req.body.multiplier, 10)
  data.push(item)
  LOG.info(`SAVING NEW estimate ${JSON.stringify(item)}`)
  return res.redirect('/estimate')
})


//update    -- Unit 11 -- team 2    
api.post('/save/:id', passport.isAuthenticated, (req, res) => {
  console.log(req)
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.estimates.query;
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  item.name = req.body.name
  item.location = req.body.location
  item.squareFeet = parseInt(req.body.sqft, 10)
  item.materials = []
  item.materials.length = 0
  if (req.body.materialName.length > 0) {
    for (let count = 0; count < req.body.materialName.length; count++) {
      item.materials.push(
        {
          product: req.body.materialName[count],
          unitcost: req.body.materialUnitCost[count],
          coverageSquareFeetPerUnit: req.body.materialSqft[count]
        }

      )
    }
   
  }
  item.numberOfPeople = parseInt(req.body.numberOfPeople, 10);
  item.numberOfDays = parseInt(req.body.numberOfDays, 10);
  item.hoursWorkedPerDay = parseInt(req.body.hoursWorkedPerDay, 10);
  item.laborDollarsPerHour = parseInt(req.body.laborDollarsPerHour, 10);
  item.numberHotelNights = parseInt(req.body.numberHotelNights, 10);
  item.numberHotelRooms = parseInt(req.body.numberHotelRooms, 10);
  item.hotelDollarsPerNight = parseInt(req.body.hotelDollarsPerNight, 10);
  item.foodDollarsPerDay = parseInt(req.body.foodDollarsPerDay, 10);
  item.numberOfVehicles = parseInt(req.body.numberOfVehicles, 10);
  item.milesPerVehicle = parseInt(req.body.milesPerVehicle, 10);
  item.dollarsPerMile = parseInt(req.body.dollarsPerMile, 10);
  item.miscellaneous = []
  item.miscellaneous.length = 0
  if (req.body && req.body.misc && req.body.misc.length > 0) {
    for (let count = 0; count < req.body.misc.length; count++) {
      item.miscellaneous.push(
        {
          misc: req.body.misc[count],
          cost: parseInt(req.body.miscCost[count], 10)

        }
      )
    }
  }
  item.multiplier = parseInt(req.body.multiplier, 10)
   LOG.info(`SAVING UPDATED estimate ${JSON.stringify(item)}`)
    return res.redirect('/estimate')
})


//delete  -- Unit 5 -- team 5
api.post('/delete/:id', passport.isAuthenticated, function (req, res) {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id, 10) // base 10
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.estimates.query
  const item = find(data, { _id: id })
  if (!item) {
    return res.end(notfoundstring)
  }
  if (item.isActive) {
    item.isActive = false
    console.log(`Deacctivated item ${JSON.stringify(item)}`)
  } else {
    const item = remove(data, { _id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  }
  return res.redirect('/estimate')
})

//select   -- Unit 6 -- team 6         
api.get('/select', passport.isAuthenticated, function (req, res) {
  res.render('estimate/select.ejs')
})

//copyfrom    -- Unit 9  -- team 4  
api.post('/copyfrom/:id', function (req, res) { });

//print   -- Unit 10 -- team 3
api.get('/print/:id', function (req, res) { })

module.exports = api
